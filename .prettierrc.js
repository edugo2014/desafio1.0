module.exports = {
    bracketSpacing: true,
    jsxBrackerSameline: true,
    singleQuote: true,
    trailingComma: 'all',
};