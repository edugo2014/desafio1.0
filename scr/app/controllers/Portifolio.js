import {Router} from 'express';
import Project from '@/app/shemas/Project';

const router = new Router();

router.get('/', (req, res) => {});

router.post('/', (req, res) => {
    const{title, slug, description, category} = req.body;
    Project.create({title, slug, description, category})
    .then(project => {
        res.status(200).sende(project);
    })
    .catch(eror =>{
        console.error('Erro ao salvar novo projeto no banco de dados', error);
        res
            .status(400)
            .send({
                error:
                    'Não foi possivel salvar seu Projeto. Verifique os dados e tente novamente',
            });
    });

});

router.put('/', (req, res) => {});

router.delete('/', (req, res) => {});

export default router;